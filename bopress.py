import os
import sys
import webbrowser

import tornado.web
from bopress import coreplugins
from bopress import settings
from bopress.handlers import urls
from bopress.hook import Hooks, do_action
from bopress.log import Logger
from bopress.utils import Utils
from tornado import httpserver
from tornado import ioloop
from tornado import options

from bopress.orm import SessionFactory

__author__ = 'tinyms'
settings.BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PORT = 9090
settings.PLUGINS_ROOT = os.path.join(settings.BASE_DIR, "plugins")
Utils.mkdirs(settings.PLUGINS_ROOT)
sys.path.append(settings.PLUGINS_ROOT)
Hooks.load(settings.PLUGINS_ROOT, coreplugins.load)
do_action("bo_settings", settings)
Logger.init()
SessionFactory.create_tables()
do_action("bo_urlmapping", urls)
if __name__ == '__main__':
    options.parse_command_line()
    app = tornado.web.Application(
        debug=settings.DEBUG,
        handlers=urls,
        static_path=os.path.join(settings.BASE_DIR, "static"),
        template_path=os.path.join(settings.BASE_DIR, "template"),
        # ui_modules=[ui],
        cookie_secret=settings.COOKIE_SECRET,
        xsrf_cookies=settings.XSRF_COOKIES,
    )
    coreplugins.init_data()
    do_action("bo_tornado_server", app)
    print("Tornado %s running %i.." % (tornado.version, PORT))
    http_server = httpserver.HTTPServer(app)
    http_server.listen(PORT, "127.0.0.1")
    if settings.DEBUG:
        webbrowser.open_new_tab("http://127.0.0.1:%i" % PORT)
    ioloop.IOLoop.instance().start()
