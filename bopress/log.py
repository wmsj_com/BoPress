import logging
import os
from logging.handlers import RotatingFileHandler

from bopress.utils import Utils
from bopress import settings

C_LOG_SIZE = 1048576
C_LOG_NAME = os.path.join(settings.BASE_DIR, "BoPress.log")
C_LOG_FILES = 3

TAG = "bopress"


class Logger(object):
    is_inited = False

    @staticmethod
    def init():
        rt = RotatingFileHandler(filename=C_LOG_NAME, maxBytes=C_LOG_SIZE, backupCount=C_LOG_FILES)
        rt.setLevel(logging.ERROR)
        rt.setFormatter(logging.Formatter('[%(asctime)s] %(message)s'))
        logging.getLogger().addHandler(rt)
        Logger.is_inited = True

    @staticmethod
    def error(obj, tag=TAG):
        logging.error("[ERROR] %s: %s" % (tag, Utils.encode(obj)))

    @staticmethod
    def exception(e, tag=TAG):
        logging.error("[EXCEPTION] %s: %s" % (tag, str(e)))

    @staticmethod
    def info(obj, tag=TAG):
        logging.error("[INFO] %s: %s" % (tag, Utils.encode(obj)))
