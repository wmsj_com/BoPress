from tornado.escape import to_unicode
from wtforms import Form, StringField, validators, IntegerField


class FormData(object):
    def __init__(self, handler):
        self._wrapped = handler.request.arguments

    def __iter__(self):
        return iter(self._wrapped)

    def __len__(self):
        return len(self._wrapped)

    def __contains__(self, name):
        return name in self._wrapped

    def getlist(self, name):
        try:
            return [to_unicode(v) for v in self._wrapped[name]]
        except KeyError:
            return []


class UserRegistryFrom(Form):
    user_email = StringField("Email", validators=[validators.input_required("必须填写"),
                                                  validators.email("邮箱格式错误")])
    user_pass = StringField("UserPass", validators=[validators.input_required("必须填写"),
                                                    validators.length(min=8, message="至少8个字母数字或下划线")])
    user_re_pass = StringField("UserPass2", validators=[validators.input_required("必须填写"),
                                                        validators.equal_to('user_pass', "密码不一致")])


class UserForm(Form):
    user_login = StringField("UserLogin", validators=[validators.input_required("必须填写")])
    user_nicename = StringField("UserNiceName")
    user_email = StringField("UserEmail", validators=[validators.input_required("必须填写"),
                                                      validators.email("邮箱格式错误")])
    user_mobile_phone = StringField("UserMobilePhone")
    display_name = StringField("DisplayName")
    user_status = IntegerField("UserStatus")


class UserLoginFrom(Form):
    user_email = StringField("Email", validators=[validators.input_required("必须填写")])
    user_pass = StringField("UserPass", validators=[validators.input_required("必须填写")])


class ProfileForm(Form):
    # user_login = StringField("user_login")
    user_email = StringField("email")
    user_nicename = StringField("nick_name")
    display_name = StringField("display_name")
    description = StringField("description")
    gravatar = StringField("gravatar")
    new_pass = StringField("new_pass",
                           validators=[validators.length(min=8, message="至少8个字母数字或下划线"),
                                       validators.optional()])
