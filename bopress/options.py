from bopress.cache import Cache
from bopress.model import Options
from bopress.log import Logger
from bopress.orm import SessionFactory


# 站点及站点模块参数配置

# 选项值包装类
class OptionValue(object):
    __value__ = None

    def __init__(self, values):
        if not values:
            self.__value__ = dict()
        else:
            self.__value__ = values

    def get(self, opt_key, single=True, default=""):
        """
        得到选项值，返回单值或者多值，主要针对Admin Option Form
        :param default:
        :param opt_key:
        :param single:
        :return:
        """
        if single:
            return self.__value__.get(opt_key, [default])[0]
        else:
            return self.__value__.get(opt_key, default)

    def empty(self):
        if len(self.__value__.keys()) == 0:
            return True
        return False


def loads():
    """

    """
    Cache.default().delete(["bo_options"])
    s = SessionFactory.session()
    ds = s.query(Options).filter(Options.autoload == "yes").all()
    for c in ds:
        Cache.default().set(["bo_options", c.option_name], c.option_value)


def save_options(option_group_name, option_value, auto_load="yes"):
    """
    保存选项值
    :param option_group_name: 选项组名称， 需全局唯一
    :param option_value: 能序列化与反序列化的object
    :param auto_load: `yes` or `no`
    :return: option value
    """
    s = SessionFactory.session()
    o = s.query(Options).filter(Options.option_name == option_group_name).one_or_none()
    try:
        if o:
            o.option_value = option_value
            s.commit()
        else:
            o = Options()
            o.option_name = option_group_name
            o.option_value = option_value
            o.autoload = auto_load
            s.add(o)
            s.commit()
        Cache.default().delete(["bo_options", o.option_name])
    except Exception as e:
        s.rollback()
        Logger.exception(e)

    return o


def get_options(option_group_name):
    """
    得到选项值
    :param option_group_name: 选项组名称， 需全局唯一
    :return: option value
    """
    v = Cache.default().get(["bo_options", option_group_name])
    if v:
        return v
    s = SessionFactory.session()
    v = s.query(Options.option_value).filter(Options.option_name == option_group_name).scalar()
    if v:
        # 0 永久缓存
        Cache.default().set(["bo_options", option_group_name], v, 0)
        return v
    return dict()


def get_form_options(option_group_name):
    """
    得到由Admin选项表单保存的选项值
    :param option_group_name: 选项组名称， 需全局唯一
    :return: OptionValue
    """
    v = Cache.default().get(["bo_options", option_group_name])
    if v:
        return OptionValue(v)
    s = SessionFactory.session()
    v = s.query(Options.option_value).filter(Options.option_name == option_group_name).scalar()
    if v:
        # 0 永久缓存
        Cache.default().set(["bo_options", option_group_name], v, 0)
    return OptionValue(v)


def delete_options(option_group_name):
    """
    删除选项组
    :param option_group_name:选项组名称， 需全局唯一
    """
    s = SessionFactory.session()
    s.query(Options).filter(Options.option_name == option_group_name).delete()
    s.commit()
    Cache.default().delete(["bo_options", option_group_name])


def get_site_options():
    """
    得到全局站点选项，即Admin后台设置下的通用选项
    :return:
    """
    return get_form_options("bo-site-settings")
