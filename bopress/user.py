import arrow
import sqlalchemy as sa

from bopress import metas, options
from bopress.log import Logger
from bopress.model import Users
from bopress.orm import SessionFactory
from bopress.utils import Utils, DataResult


def login(user_key, user_pass):
    """
    用户登录
    :param user_key: value in [user_login,user_email,user_mobile_phone]
    :param user_pass: password
    :return: bopress.model.Users
    """
    s = SessionFactory.session()
    u = s.query(Users).filter(
        sa.or_(Users.user_login == user_key, Users.user_email == user_key, Users.user_mobile_phone == user_key)) \
        .filter(Users.user_pass == Utils.md5(user_pass)) \
        .filter(Users.user_status == 1).one_or_none()
    r = DataResult()
    if u:
        r.success = True
        r.data = u
        return r
    r.success = False
    r.message = "401"
    return r


def get(user_id=-1, user_login="", user_email="", user_mobile_phone=""):
    """
    得到用户, 只要以下任意参数赋值
    :param user_id: user id
    :param user_login: user login
    :param user_email: email
    :param user_mobile_phone: mobile phone
    :return: bopress.model.Users
    """
    s = SessionFactory.session()
    if user_id > 0:
        return s.query(Users).filter(Users.user_id == user_id).one_or_none()
    if user_login:
        return s.query(Users).filter(Users.user_login == user_login).one_or_none()
    if user_email:
        return s.query(Users).filter(Users.user_email == user_email).one_or_none()
    if user_mobile_phone:
        return s.query(Users).filter(Users.user_mobile_phone == user_mobile_phone).one_or_none()
    return None


def registry(user_login, user_pass, user_email="", user_mobile_phone="", user_nicename="", display_name="",
             user_status=0):
    """
    注册新用户
    :param user_login: user login
    :param user_pass: password
    :param user_email: email
    :param user_mobile_phone: mobile phone
    :param user_nicename: nice name
    :param display_name: display name
    :param user_status: status 0 or 1
    :return: DataResult
    """
    r = DataResult()
    s = SessionFactory.session()
    num = s.query(Users).filter(Users.user_login == user_login).count()
    if num > 0:
        r.success = False
        r.message = "UserLoginExists"
        return r
    if user_mobile_phone:
        num = s.query(Users).filter(Users.user_mobile_phone == user_mobile_phone).count()
        if num > 0:
            r.success = False
            r.message = "UserMobilePhoneExists"
            return r
    if user_email:
        num = s.query(Users).filter(Users.user_email == user_email).count()
        if num > 0:
            r.success = False
            r.message = "UserEmailExists"
            return r
    u = Users()
    try:
        u.user_login = user_login
        u.user_pass = Utils.md5(user_pass)
        u.user_email = user_email
        u.user_mobile_phone = user_mobile_phone
        u.user_nicename = user_nicename
        u.display_name = display_name
        u.user_status = user_status
        u.user_activation_key = Utils.uniq_index()
        u.user_registered = Utils.current_datetime()
        s.add(u)
        s.commit()
        site_options = options.get_site_options()
        metas.save_user_metas(u.user_id, "bo_super", False)
        metas.save_user_metas(u.user_id, "bo_roles", set(site_options.get("default_role", False)))
        metas.save_user_metas(u.user_id, "bo_description", "")
        metas.save_user_metas(u.user_id, "bo_gravatar", "")
        r.success = True
        r.message = ""
        r.data = u
        return r
    except Exception as e:
        Logger.error(str(e))
        r.success = False
        r.message = "500"
        return r
