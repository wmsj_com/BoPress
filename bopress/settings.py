# 调试
DEBUG = True
# 应用根目录
BASE_DIR = ""
# 插件目录
PLUGINS_ROOT = ""
# Cookie 安全 tornado
COOKIE_SECRET = "xxxxxx"
# session 超时(秒)
SESSION_TIMEOUT = 300
# xsrf 保护
XSRF_COOKIES = False
# 缓存方式 `simple` `file`, `mem`, `redis`
CACHE_MODE = "simple"
# 数据表前缀
TABLE_NAME_PREFIX = "bo_"
# 数据库连接
DB_CONNECT_URI = "sqlite:///bopress.sqlite3"
