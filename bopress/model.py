from sqlalchemy import Column, String, BigInteger, PickleType, DateTime, Integer

from bopress.orm import Entity, many_to_one


class Options(Entity):
    option_id = Column(BigInteger().with_variant(Integer, "sqlite"), autoincrement=True, nullable=False, primary_key=True)
    option_name = Column(String(255), nullable=False, default="")
    option_value = Column(PickleType, nullable=False)
    # yes or no
    autoload = Column(String(20), nullable=False, default="yes")


class Users(Entity):
    user_id = Column(BigInteger().with_variant(Integer, "sqlite"), autoincrement=True, nullable=False, primary_key=True)
    user_login = Column(String(100), nullable=False, unique=True)
    user_pass = Column(String(255), nullable=False)
    user_nicename = Column(String(50), nullable=False, default='')
    user_email = Column(String(100), nullable=False, default='')
    user_mobile_phone = Column(String(20), nullable=False, default='')
    user_url = Column(String(100), nullable=False, default='')
    user_registered = Column(DateTime, nullable=False)
    user_activation_key = Column(String(255), nullable=False, default='')
    user_status = Column(Integer, nullable=False, default=0)
    display_name = Column(String(255), nullable=False, default='')


@many_to_one(Users)
class UserMeta(Entity):
    meta_id = Column(BigInteger().with_variant(Integer, "sqlite"), autoincrement=True, nullable=False, primary_key=True)
    meta_key = Column(String(255), nullable=False, default='')
    meta_value = Column(PickleType, nullable=False)
