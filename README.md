# 简介 #
BoPress，使用Python语言编写的Web后台框架，全插件式驱动，深度借鉴WordPress，但不致力博文开发，更倾向于锻造一个稳固的通用后台，系统仅提供用户，角色，权限，认证，选项，缓存，插件系统等基础组件。

BoPress 主要依赖以下开源项目

1. Tornado
2. SQLAlchemy
3. AdminLTE

# 运行 #
安装依赖

pip install -r requirements.txt

>默认使用SQLite数据库，使用其它数据库请修改plugins/bocore/meta.py内的数据库连接配置

运行 `bopress.py`， super，super 登录后台

[文档](doc/SUMMARY.md)

[屏幕截图](https://git.oschina.net/yezang/BoPress/wikis/screenshots)

[开发环境](https://git.oschina.net/yezang/BoPress/wikis/dev)

<img src="http://git.oschina.net/uploads/images/2017/0225/183850_5e849bea_1222223.png" alt="">