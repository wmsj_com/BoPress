import tornado.gen
import tornado.web
from tornado.web import url

from bopress import options
from bopress.handlers import BaseHandler
from bopress.hook import add_action, add_filter, add_option_page, do_action
from oauth2.vendor import QQAuth2Minix, WeiboMixin
from bopress.log import Logger

plugin_name = "OAuth2"
version = "1.0"


class BoQQOAuth2Handler(BaseHandler, QQAuth2Minix):
    @tornado.gen.coroutine
    def get(self):
        s = options.get_form_options("bo-oauth2-settings")
        if self.get_argument('code', False):
            user = yield self.get_authenticated_user(
                redirect_uri=s.get("qq_return_uri", default=""),
                code=self.get_argument('code'))
            Logger.info(user, "BoQQOAuth2Handler")
            do_action("bo_oauth2", user)
        else:
            yield self.authorize_redirect(
                redirect_uri=s.get("qq_return_uri", default=""),
                client_id=s.get("qq_appid", default=""),
                client_secret=s.get("qq_appkey", default=""),
                response_type='code',
                extra_params={'approval_prompt': 'force'})


class BoWeiboAuthHandler(BaseHandler, WeiboMixin):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        s = options.get_form_options("bo-oauth2-settings")
        if self.get_argument('code', None):
            user = yield self.get_authenticated_user(
                redirect_uri=s.get("weibo_return_uri", default=""),
                client_id=s.get("weibo_appid", default=""),
                client_secret=s.get("weibo_appkey", default=""),
                code=self.get_argument('code'))
            Logger.info(user, "BoWeiboAuthHandler")

        else:
            self.authorize_redirect(
                redirect_uri=s.get("weibo_return_uri", default=""),
                client_id=s.get("weibo_appid", default="")
            )


def handlers(urls):
    urls.append(url(r"/bopress/oauth2/qq", BoQQOAuth2Handler, name="qq-oauth2"))
    urls.append(url(r"/bopress/oauth2/weibo", BoWeiboAuthHandler, name="weibo-oauth2"))


def loginform_footer(htmls, handler):
    html = """
    <div class="social-auth-links">合作登录
    <a href="{}">QQ</a> <a href="{}">微博</a>
    </div>
    """.format(handler.reverse_url("qq-oauth2"), handler.reverse_url("weibo-oauth2"))
    htmls.append(html)
    return htmls


def oauth2_form(form, screen_id, handler):
    if screen_id == "bo-oauth2-settings":
        form = """
                <h4>QQ 登录</h4>
                <div class="row">
                    <div class="col-xs-3">
                        <label for="site_url">App Id</label>
                        <input type="text" class="form-control option" id="qq_appid" name="opt_qq_appid" placeholder="">
                    </div>
                    <div class="col-xs-3">
                        <label for="site_url">App Key</label>
                        <input type="text" class="form-control option" id="qq_appkey" name="opt_qq_appkey" placeholder="">
                    </div>
                    <div class="col-xs-6">
                        <label for="site_url">回调地址</label>
                        <input type="text" class="form-control option" id="qq_return_uri" name="opt_qq_return_uri" placeholder="">
                    </div>
                </div>

                <h4>微博 登录</h4>
                <div class="row">
                    <div class="col-xs-3">
                        <label for="site_url">App Id</label>
                        <input type="text" class="form-control option" id="weibo_appid" name="opt_weibo_appid" placeholder="">
                    </div>
                    <div class="col-xs-3">
                        <label for="site_url">App Key</label>
                        <input type="text" class="form-control option" id="weibo_appkey" name="opt_weibo_appkey" placeholder="">
                    </div>
                    <div class="col-xs-6">
                        <label for="site_url">回调地址</label>
                        <input type="text" class="form-control option" id="weibo_return_uri" name="opt_weibo_return_uri" placeholder="">
                    </div>
                </div>

                <h4>微信 登录</h4>
                <div class="row">
                    <div class="col-xs-3">
                        <label for="site_url">App Id</label>
                        <input type="text" class="form-control option" id="wechat_appid" name="opt_wechat_appid" placeholder="">
                    </div>
                    <div class="col-xs-3">
                        <label for="site_url">App Key</label>
                        <input type="text" class="form-control option" id="wechat_appkey" name="opt_wechat_appkey" placeholder="">
                    </div>
                    <div class="col-xs-6">
                        <label for="site_url">回调地址</label>
                        <input type="text" class="form-control option" id="wechat_return_uri" name="opt_wechat_return_uri" placeholder="">
                    </div>
                </div>
                """
    return form


add_action("bo_urlmapping", handlers)
add_filter("bo_user_loginform_footer", loginform_footer)
add_option_page("OAuth2 第三方登录", "OAuth2", "bo-oauth2-settings", ["manage_options"])
add_filter("bo_options_general_form", oauth2_form)
