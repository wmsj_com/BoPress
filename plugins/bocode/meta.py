from bopress.hook import add_action, add_code_generation_menu_page, \
    add_filter, add_code_generation_submenu_page
from bocode.data import DataBase

plugin_name = "Code Builder"
version = "1.0"

add_code_generation_menu_page("代码", "代码", "code-generic")
add_code_generation_submenu_page("code-generic", "WordPress DataTable", "WP DataTable", "code-wordpress")
add_code_generation_submenu_page("code-generic", "JQuery DataTables", "JQuery DataTables", "code-jquery-datatables")
add_code_generation_submenu_page("code-generic", "Html Forms", "Html Forms", "code-html-forms")

add_action("bo_api_post_code_generation", DataBase.request)


def wordpress_code_generation_properties_buttons(btns, screen_id, hanlder):
    if screen_id == "code-wordpress":
        btns.append(("DataTable", "code/wp_table.tpl"))
    return btns


add_filter("bo_code_generation_buttons", wordpress_code_generation_properties_buttons)


def jquery_datatables_code_generation_properties_buttons(btns, screen_id, hanlder):
    if screen_id == "code-jquery-datatables":
        btns.append(("DataTable", "code/jquery-datatables.tpl"))
    return btns


add_filter("bo_code_generation_buttons", jquery_datatables_code_generation_properties_buttons)


def jquery_datatables_code_generation_properties_form(html_form_str, screen_id, hanlder):
    if screen_id == "code-jquery-datatables":
        html_form_str = """
                <div class="form-group">
                  <label>固定表头</label>
                  <select class="form-control" name="opt_fix_table_header">
                    <option value="N">否</option>
                    <option value="Y">是</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>冻结列</label>
                  <select class="form-control" name="opt_freeze_table_columns">
                    <option value="N">否</option>
                    <option value="Y">是</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>列搜索</label>
                  <select class="form-control" name="opt_table_column_search">
                    <option value="N">否</option>
                    <option value="Y">是</option>
                  </select>
                </div>
        """
    return html_form_str


add_filter("bo_code_generation_properties_form", jquery_datatables_code_generation_properties_form)


def htmlforms_code_generation_properties_buttons(btns, screen_id, hanlder):
    if screen_id == "code-html-forms":
        btns.append(("AdminLTE", "code/forms-adminlte.html"))
    return btns


add_filter("bo_code_generation_buttons", htmlforms_code_generation_properties_buttons)


def htmlforms_datatables_code_generation_properties_form(html_form_str, screen_id, hanlder):
    if screen_id == "code-html-forms":
        html_form_str = """
                <div class="form-group">
                  <label>列数</label>
                  <select class="form-control" name="opt_column_nums">
                    <option value="1">1列</option>
                    <option value="2">2列</option>
                    <option value="3">3列</option>
                    <option value="4">4列</option>
                  </select>
                </div>
        """
    return html_form_str


add_filter("bo_code_generation_properties_form", htmlforms_datatables_code_generation_properties_form)
