from bopress.hook import add_action

plugin_name = "BoPress Core"
version = "1.0"


def bo_settings(settings):
    settings.DEBUG = True
    settings.DB_CONNECT_URI = "sqlite:///bopress.sqlite3"
    # settings.DB_CONNECT_URI = "mysql+pymysql://root:@127.0.0.1:3306/bopress?charset=utf8"


add_action("bo_settings", bo_settings)
