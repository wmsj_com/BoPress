# 简介 #
BoPress，使用Python语言编写的Web后台框架，全插件式驱动，深度借鉴WordPress，但不致力博文开发，更倾向于锻造一个稳固的通用后台，系统仅提供用户，角色，权限，认证，选项，缓存，插件系统等基础组件。

BoPress 主要依赖以下开源项目

1. Tornado
2. SQLAlchemy
3. AdminLTE