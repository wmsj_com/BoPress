# Tornado RequestHandler #

使用Tornado RequestHandler，实现`bo_urlmapping`动作

    from bopress.handlers import BaseHandler
	from tornado.web import url

	class SomeHandler(BaseHandler):
		def get(self, v1, v2):
			pass

	def mapping(urls):
		urls.append(url(r'/some', SomeHandler, 'some'))

	add_action('bo_urlmapping', mapping)