# 选项 #

站点及站点模块的选项值保存与获取请参阅`bopress.options`模块，需注意选项值是`Pickle`类型，值可序列化与反序列化，其对应的数据表的列数据类型为`Blob`。

## 选项页 ##
在后台添加一个选项页

    from bopress.hook import add_option_page, add_filter

	add_option_page("SMTP 邮件", "SMTP 邮件", "bo-smtp-server-settings", ["manager_option"])


## 选项表单 ##
为此选项页添加选项表单，表单控件需要一些约定，name属性以`opt_`开头，class属性增加一个`option`样式名称。

    def smtp_server_settings_form(form, screen_id, handler):
	    if screen_id == "bo-smtp-server-settings":
	        form = """
	        <div class="form-group">
	          <label for="smtp_server">服务器</label>
	          <input type="text" class="form-control option" id="smtp_server" name="opt_smtp_server" placeholder="">
	        </div>
	        <div class="form-group">
	          <label for="smtp_port">端口</label>
	          <input type="number" class="form-control option" id="smtp_port" name="opt_smtp_port" placeholder="端口">
	        </div>
	        <div class="form-group">
	          <label for="smtp_auth">账号认证</label>
	          <select class="form-control option" id="smtp_auth" name="opt_smtp_auth">
	          <option value="Y">是</option>
	          <option value="N">否</option>
	          </select>
	        </div>
	        <div class="form-group">
	          <label for="smtp_security">安全连接</label>
	          <select class="form-control option" id="smtp_security" name="opt_smtp_security">
	          <option value="">无</option>
	          <option value="ssl">ssl</option>
	          <option value="tls">tls</option>
	          </select>
	        </div>
	        <div class="form-group">
	          <label for="smtp_user_name">账号</label>
	          <input type="text" class="form-control option" id="smtp_user_name" name="opt_user_name" placeholder="账号">
	        </div>
	        <div class="form-group">
	          <label for="smtp_user_pass">密码</label>
	          <input type="password" class="form-control option" id="smtp_user_pass" name="opt_user_pass" placeholder="密码">
	        </div>
	        <div class="form-group">
	          <label for="smtp_from">发件人地址</label>
	          <input type="email" class="form-control option" id="smtp_from" name="opt_smtp_from" placeholder="发件人地址">
	        </div>
	        <div class="form-group">
	          <label for="smtp_from_name">发件人姓名</label>
	          <input type="text" class="form-control option" id="smtp_from_name" name="opt_smtp_from_name" placeholder="发件人姓名">
	        </div>
	        """
	    return form

	add_filter("bo_options_general_form", smtp_server_settings_form)

## 选项组名称 ##
默认`menu_slug`作为选项组名称，即上面SMTP配置的选项组名称是`bo-smtp-server-settings`，如果想要一个不同的选项组名称：

    def smtp_server_settings_options_group_name(group_name, screen_id, handler):
	    if screen_id == "bo-smtp-server-settings":
	        group_name = "smtp-settings"
	    return group_name

	add_filter("bo_options_general_name", smtp_server_settings_options_group_name)

## 获取表单选项值 ##


    v = get_form_options("smtp-settings")
	smtp_server = v.get("smtp_server", single=True)

## 选项组默认值 ##
初始化选项组值可在`bo_init`扩展点内进行，此扩展点在Tornado服务器启动并准备接受Http请求之前执行。

    def init():
		v = get_form_options("smtp-settings")
		if not v:
			a=dict()
			a['smtp_server']=['smtp@example.com']
			...
			save_options('smtp-settings', a)
	
	add_action('bo_init', init)

初始化的选项值必须是数组，如果不是提供给后台进行可视化配置，则不需要。
> SMTP 选项设置已内置，登录系统可作为样例参考。