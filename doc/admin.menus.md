# 后台菜单 #

后台用户菜单示例

	from bopress.hook import add_menu_page,add_submenu_page
    
	add_menu_page("用户", "用户", "bo-users", ["read"], "admin/blank.html", "", 99)

	# http://127.0.0.1:9090/bopress/admin?page=bo-users-list
    add_submenu_page("bo-users", "用户管理", "所有用户", "bo-users-list", ["read"], bo_users_page)

	# http://127.0.0.1:9090/bopress/admin?page=bo-role-capabilities
    add_submenu_page("bo-users", "角色权限", "角色权限", "bo-role-capabilities", ["read"], "admin/user_role_capabilities.html")

向后台添加菜单可用`add_menu_page`、`add_submenu_page`两函数来拓展，所有通过此方式添加菜单或者链接的都是这种URL结构 `/bopress/admin?page=menu_slug`

    add_menu_page(page_title, menu_title, menu_slug='',
                  capability=list(), cb=None, icon_url='', position=10, place="menu")

	add_submenu_page(parent_slug, page_title, menu_title, menu_slug='', capability=list(),
                     cb=None, icon_url='', position=10, place="menu")

**部分参数说明**

`menu_slug`:
菜单ID，唯一

`place`: 菜单显示处。
默认是具有层级结构的导航菜单`menu`，如果想定义的菜单项不显示在导航菜单内，`place`的值非`menu`即可。 当然也不要随便定义，具有意义是值得倡议的，在其它页面内通过此值过滤出具有层级关系的其它菜单呈现形式。非`menu`时，还需自行根据capability控制菜单访问权限。

`cb`:
str/func
点击菜单或者向此链接发送请求时，触发的动作，可以返回任何内容。

当`cb`为字符串时，则系统会把它作为模板路径来渲染此模板，比如 `admin/user_role_capabilities.html`

当`cb`为函数时，函数执行后，可以返回任意文档类型。但是大部分时间我们在模板加载之前会读取一些数据，然后传递给模板去呈现，那么需要注意，如果模板继承了系统模板`master.html`，则
`current_menu_item`, `screen_id`，需要再次传递。

    def bo_users_page(handler, current_menu_item, screen_id):
	    c = options.get_options("bo_roles")
	    handler.render("admin/users.html", current_menu_item=current_menu_item, screen_id=screen_id, roles=c)

`capability`:
访问权限
