# 角色与权限点 #

系统默认有`super`与`subscriber`两个角色及以下权限

1. read
2. list_users
3. create_users
4. edit_users
5. delete_users
6. upload_files
7. edit_files
8. manage_options

使用示例

    from bopress.hook import add_action, add_capability, add_role
	
	def init():
		add_capability('edit_post', 'post', '编辑文章')
		add_role('editor', '编辑',  ['edit_post'])
	
	add_action('bo_init', init)

在`bo_init`中初始化权限及角色，新增角色及角色权限指定也可以在后台操作。

权限验证

	auth(caps=set(), is_redirect=False, is_api=True)
	
	if handler.auth({'edit_post'}):
		do something..

在`ajax`或者菜单`cb`内可通过`handler.auth()`来判断当前用户是否有访问权限，在tornado自带模板内可以直接使用`auth()`，此函数已内置到模板。 请参阅`bopress.handler.BaseHandler`类获得更详尽的信息。